/*--------------------------------------------------------------
   Utility macro to load a table to into cas where the table
      exists on disk in the path the caslib points to.

   If spray=1 then, if we load the table, we will also have cas 
   shuffle it to make sure it is evenly distributed.

   Example:
    %dam_load_table(reltable="mytable.sas7bdat", caslib="myCaslib");
    %dam_load_table(reltable="subdir/mytable.sas7bdat", caslib="myCaslib");
 *--------------------------------------------------------------*/

%macro dam_load_table(relTable=, caslib=, replace=0, spray=0, where=);

%local tableName;
%if not %dam_is_blank(where) %then %do;
    %global __alt_where__;
    %let __alt_where__=%superq(where);
%end;

proc cas;
  /* remove preceding path and extension */
  loadedName = upcase(translate(&relTable,'.','/'));
  ix = index(loadedName,'.SAS7BDAT');
  if ix>0 then loadedName = substr(loadedName,1,ix-1);
  else do;
      ix = index(loadedName,'.SASHDAT');
      if ix>0 then loadedName = substr(loadedName,1,ix-1);
  end;
  call symputx("tableName", loadedName, 'L');

  %if not %dam_is_blank(where) %then %do;
      filter=symget("__alt_where__");
      rc=symdel("__alt_where__");
  %end;

  action table.tableinfo result=r status=s / caslib=&caslib;
  hasTables = findTable(r); 
  if hasTables then do;
     isLoaded = hasTables.where(Name=loadedName)[,{'Name'}].nrows > 0;
  end;
  else do;
     isLoaded = FALSE;
  end;
  if !isLoaded then do;
     print "NOTE: Loading " &relTable " as " loadedName " in caslib " &caslib.;
     action table.loadTable / 
         caslib=&caslib path=&relTable casout={caslib=&caslib} 
         %if not %dam_is_blank(where) %then %do;
             where=filter
         %end;;
  end;
  else if &replace = 1 then do;
     print "NOTE: Reloading " &relTable " as " loadedName " in caslib " &caslib.;
     action table.dropTable / 
         caslib=&caslib name=loadedName  quiet = TRUE;
     action table.loadTable / 
         caslib=&caslib path=&relTable casout={caslib=&caslib} 
         %if not %dam_is_blank(where) %then %do;
             where=filter
         %end;;
  end;
  else do;
     print "NOTE: " &relTable " is already loaded as " loadedName " in caslib " &caslib.;
  end;
quit;

/*
  proc casutil outcaslib="&caslib.";
     load data= &dsTable. replace;
  quit;
*/


/* Shuffle the data so we get a good transfer if needed. Move this to load! */
%if &spray eq 1 %then %do;
  proc cas;
    action table.shuffle 
      table = { name="&tableName." caslib=&caslib } 
      casout = {
        name="&tableName." caslib=&caslib
        replace=TRUE
        replication=0
      } 
    ;
  run; quit;
%end;

%mend dam_load_table;
