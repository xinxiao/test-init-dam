/*
 Copyright (C) 2022 SAS Institute Inc. Cary, NC, USA
*/

/**
   \file
   \anchor  pcpr_init_cas
   \brief   Utility macro to reconnect to the single cas session that the analytics flow is sharing.

   \param [in] cas_session_ref              Cas session reference name
   \param [in] cas_session_uuid             The UUID for the cas session
   \param [in] sessopts                     Session options for starting the session
   \param [in] token                        Token used for connect to cas
   
   
   
   \details Initialize cas session

   \ingroup Installation
   \author  SAS Institute Inc.
   \date    2022
*/

%macro pcpr_init_cas(cas_session_ref=
                    ,cas_session_uuid= 
                    ,sessopts =
                    ,token=);
                    
    /*----- turn off ods output -----*/
    ods html close;
    ods listing;
    /*-----TODO: setup debugging options -----*/

    %if %length(&token) %then %do;
        OPTIONS AUTHINFO='';
        OPTIONS SET=SAS_VIYA_TOKEN="&token";
    %end;

    %local cas_sessopts;
    %if %length(&sessopts.) %then %do;
        %let cas_sessopts=&sessopts.;
    %end;
    %else %do;
        %let cas_sessopts=%bquote(timeout=1800 caslib=casuser);
    %end;

    /*----- first time: create cas session -----*/
    %local casuser_option 
            uuid_option ;

    %let cas_session_ref = &cas_session_ref.;
    %let cas_session_uuid = &cas_session_uuid.;

    %if &cas_session_ref ne %then %do;
        %let m_cas_session_ref = &cas_session_ref.;
    %end;
    %else %do;
        %let m_cas_session_ref = mysess;
    %end;

    %if &cas_session_uuid ne %then %do;
        %global cas_session_uuid_tmp;
        %let uuid_option = %str(UUIDMAC = cas_session_uuid_tmp);
    %end;

    cas &m_cas_session_ref. sessopts=(&cas_sessopts.) &uuid_option; 
    caslib _all_ assign;

    %if &cas_session_uuid ne %then %do;
        %let &cas_session_uuid = &cas_session_uuid_tmp;
    %end;

%mend pcpr_init_cas;
