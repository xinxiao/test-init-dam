/********************************************************************************************************
       Module        :  dabt_terminate_cas_session
       Function    :  This macro terminates the cas session
    Called-by    :  SAS Macro
    Calls        :  None
       Parameters    :  cas_session_ref       -> Session name provided by invoking macro to initiate by same name
                   
*********************************************************************************************************/

%macro dabt_terminate_cas_session(cas_session_ref=);
    
    %let cas_session_ref = &cas_session_ref.;
    
    %if &cas_session_ref ne %then %do;
        %let m_cas_session_ref = &cas_session_ref.;
    %end;
    %else %do;
        %let m_cas_session_ref = mysess;
    %end;
    
cas &m_cas_session_ref. terminate;
%mend dabt_terminate_cas_session;