   1 /*-------------------------------------------------------------------
   2  * Macro to save cas table to persistant area.
   3  * Checks to make sure table actually has data because otherwise
   4  * I get weird errors.
   5  *
   6  * Parameters:
   7  *    Required
   8  *        caslib - caslib of cas table to copy
   9  *        table_from - name of cas table to copy
  10  *        table_to - name of table to persist
  11  *        ext  - extensions to persist table as (sas7bdat)
  12  *    Optional
  13  *        caslib_from - cas libname for cas table
  14  *        skipEmpty - If true, data set will not be saved if it is empty.
  15  *        fixEmpty - only used for method log tables and position error tables that are sas7bdats,
  16  *                   if empty put an "All good" message in table.
  17  *-------------------------------------------------------------------*/
  18 %macro dam_save_table(caslib_from=, caslib=, table_from=, table_to=, ext=sas7bdat, skipEmpty=false, fixEmpty=false, mvalib=);
  19   %local nobs;
  20   %let nobs=0;
  21 
  22   /* caslib_from is optional and if not passed we will assume it is the same as caslib */
  23   %if %dam_is_blank(caslib_from) %then %do;
  24       %let caslib_from=&caslib.;
  25   %end;
  26 
  27   %if %alm_is_blank(skipEmpty) %then %do;
  28       %let skipEmpty=false;
  29   %end;
  30 
  31   proc cas;
  32       table.tableexists result=results / caslib="&caslib_from." name="&table_from.";
  33       skipEmpty=&skipEmpty;
  34 
  35       if results.exists then do;
  36           table.tabledetails result=tableDetail/ caslib="&caslib_from." name="&table_from.";
  37           rows = findTable(tableDetail)[1,{'Rows'}][1];
  38           call symputx('nobs', rows, 'L');
  39           if rows=0 and skipEmpty=true then do;
  40               print "NOTE:(DAM_SAVE_TABLE) Skipped saving cas table &caslib_from..&table_from. to &caslib..&table_to..&ext. because it is empty.";
  41           end;
  42           else do;
  43               print "NOTE:(DAM_SAVE_TABLE) Saving cas table &caslib_from..&table_from. to &caslib..&table_to..&ext..";
  44               table.save /
  45                   caslib="&caslib." name="&table_to..&ext." replace="True"
  46                   table={caslib="&caslib_from." name="&table_from."};
  47           end;
  48       end;
  49       else do;
  50           print "NOTE:(DAM_SAVE_TABLE) Skipped saving cas table &caslib_from..&table_from. to &caslib..&table_to..&ext. because it does not exist.";
  51       end;
  52   run; quit;
  53 
  54 
  55   %if %upcase("&ext.") eq "SAS7BDAT" and not %dam_is_blank(mvalib) %then %do;
  56       data &mvalib..&table_to.;
  57           %if &nobs=0 and &fixEmpty=true %then %do;
  58               /*IRM PDF cannot open an empty sas7bdat. A workaround to make sure the data is not empty.
  59                 Only use for method log tables and position error tables*/
  60               message = "All Good";
  61           %end;
  62           %else %do;
  63               /*Python CFG uses panda package to read sas7bdat. Need to rewrite sas7bdat to fix the magic number
  64                 for Python CFG. Can remove when Python CFG read all input data from CAS, not from sas7bdat*/
  65               set &mvalib..&table_to.;
  66           %end;
  67       run;
  68   %end;
  69 %mend dam_save_table;
