/*  
    sample call
    options mprint mlogic symbolgen;
    %dabt_drop_table(m_table_nm=mycaslib.cars, m_cas_flg=Y);
*/

%macro dabt_drop_table(m_table_nm=, m_cas_flg=N, m_delete_cas_data_source_file=N);
    /* by default m_cas_flg=N so that existing code which calls dabt_drop_table is not affected */
    %if "&m_cas_flg"="N" %then %do; /* i18nOK:Line */
        %if %sysfunc(exist(&m_table_nm)) %then %do;
            proc sql noprint;
                    drop table &m_table_nm; 
            quit;
        %end;
        %dabt_err_chk(type=SQL);
    %end;
    %else %if "&m_cas_flg"="Y"  %then %do; /* i18nOK:Line */

    /*Split two level name having lib.table*/
    data _null_;
        dsName = "&m_table_nm";
        LocDot = findc(dsName, ".");          /* Does name contain a period (.)?     */ /* i18nOK:Line */
        if LocDot > 0 then do;                /*   Yes: it is a two-level name       */
            lib_split = substr(dsName, 1, LocDot-1); /*     get substring before the period */ /* i18nOK:Line */
            table_nm_split = substr(dsName, LocDot+1); /*     get substring after the period  *//* i18nOK:Line */
            call symputx('m_lib_split',lib_split);     /* i18nOK:Line */ 
            call symputx('m_table_nm_split',table_nm_split);    /* i18nOK:Line */
        end;
        else do;                              /*   No: it is a one-level name        */
            lib_split = "casuser";    /*     No: use WORK                    */ /* i18nOK:Line */
            table_nm_split = dsName;
            call symputx('m_lib_split',lib_split);     /* i18nOK:Line */
            call symputx('m_table_nm_split',table_nm_split);    /* i18nOK:Line */
        end;
    run;

    /* quiet=true to avoid error if table don't exist in memmory*/
    proc cas;
        table.dropTable name="&m_table_nm_split"  CASLIB="&m_lib_split" quiet=true; run;
        quit;
    %if "&m_delete_cas_data_source_file." = "Y" %then %do;  /* I18NOK:LINE */
        /*Delete physical file*/
        %let m_table_nm_split=%sysfunc(kupcase(&m_table_nm_split));
        proc cas;
            table.deleteSource source="&m_table_nm_split..sashdat"  CASLIB="&m_lib_split" quiet=true removeAccessControls=TRUE; run;  /* I18NOK:LINE */
        quit;
    %end;
%end;

%mend dabt_drop_table;