/*************************************************************************
 * Copyright 2019, SAS Institute Inc., Cary, NC, USA. All Rights Reserved.
 *
 * NAME:        dam_is_blank
 *
 * PURPOSE:     Check if a macro variable is blank
 *
 *
 * PARAMETERS: 
 *              mac_var 
 *                  <required> - a macro variable name
 *
 * EXAMPLE: %dam_is_blank(mac_var=test_run);
 **************************************************************************/
%macro dam_is_blank(mac_var);
    %sysevalf(%superq(&mac_var)=,boolean)
%mend dam_is_blank;
