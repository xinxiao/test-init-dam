/********************************************************************************************************
       Module        :  dabt_promote_table_to_cas
       Function    :  This macro promotes(to make it available to other sessions) 
                    table which is already present in caslib.
                    Promote action deletes the session scope table from input cas lib. 
                    If user want to keep the session scope table then set drop_sess_scope_tbl_flg=False
    Called-by    :  SAS Macro
    Calls        :  None
       Parameters    :  Mendatory Parameters:
                        input_caslib_nm           -> Name of input cas library from where table has to promote
                        input_table_nm            -> Name of table which has to promote
                   Optional Parameters
                        output_table_nm            -> Name of table which has to promote
                        output_caslib_nm          -> Name of output cas library where table has to promote
                        drop_sess_scope_tbl_flg -> Flag to indicate if session scope table is to be delete or not
                                                    after table is promoted.(Y/N)
     Author:    Sanchit Singla(sinsls)
     
     
    Sample Call - %dabt_promote_table_to_cas(input_caslib_nm =<caslib>,input_table_nm =<table_nm>);
                   
*********************************************************************************************************/

%macro dabt_promote_table_to_cas(input_caslib_nm =,input_table_nm =,output_caslib_nm =,output_table_nm = ,drop_sess_scope_tbl_flg=Y);
    
    %let m_input_caslib_nm = &input_caslib_nm.;
    %let m_input_tbl_nm = &input_table_nm.;
    %let m_output_caslib_nm = &output_caslib_nm.;
    %let m_output_tbl_nm = &output_table_nm.;
    %let m_drop_sess_scope_tbl_flg = &drop_sess_scope_tbl_flg.;
    
    *=============================================================================;
    * If output caslib is blank then input caslib will be treated as output caslib;
    *=============================================================================;
    
    %if &m_output_caslib_nm. eq %then %do;
        %let m_output_caslib_nm = &m_input_caslib_nm.;
    %end;
    
    *====================================================================;
    * If output table is blank then table will be promoted with same name;
    *====================================================================;
    
    %if &m_output_tbl_nm. eq %then %do;
        %let m_output_tbl_nm = &m_input_tbl_nm.;
    %end;
    
    *====================================================================;
    *Converting User Input flag Y/N to True/False;
    *====================================================================;    
    
    %if &drop_sess_scope_tbl_flg eq Y %then %do;
        %let m_drop_sess_scope_tbl_flg = TRUE;
    %end;
    %else %do;
        %let m_drop_sess_scope_tbl_flg = FALSE;
    %end;
    
    *======================================================;
    * If table is promoted to same library with same name;
    *=====================================================;
    
    %if (&m_input_caslib_nm. eq &m_output_caslib_nm.) and (&m_input_tbl_nm. eq &m_output_tbl_nm.) %then %do;
    
        *=======================================================================================;
        * To check if table is already promoted to that caslib or not. If it is then it will;
        *=======================================================================================;
        
        proc cas;
            table.tableInfo result = table_info/ caslib="&m_output_caslib_nm." name="&m_output_tbl_nm." ;
            exist_Tables = findtable(table_info);
            if exist_Tables then saveresult table_info dataout= work.table_info;
        quit;
        
        %let m_tbl_sess_scope_flg = ; 
        %let row_count = 0;
        proc sql ;
            select 
                case  
                    when global eq 1 then "True" /* i18nOK:Line */
                    else "False" /* i18nOK:Line */
                end as tbl_sess_scope_flg 
                into
                    :m_tbl_sess_scope_flg
            from work.table_info;
        quit;
        
        %let row_count = &sqlobs.;
        
        %if &row_count > 0 %then %do;
            %let m_tbl_sess_scope_flg = &m_tbl_sess_scope_flg.;
            %if &m_tbl_sess_scope_flg eq True %then %do;
                %dabt_drop_table(m_table_nm=&m_output_caslib_nm..&m_output_tbl_nm., m_cas_flg=Y);
            %end;
        %end;
        
        %let m_drop_sess_scope_tbl_flg = TRUE;
    %end;
    
    %else %do;
        *=======================================================================================;
        * To drop output table from caslib in which table is to be promoted if it already exists;
        *=======================================================================================;
        %dabt_drop_table(m_table_nm=&m_output_caslib_nm..&m_output_tbl_nm., m_cas_flg=Y);
    %end;
    *=============================;
    * CAS Action- to promote table;
    *=============================;
    
    proc cas;
        table.promote  /
        caslib = "&m_input_caslib_nm.",
        name="&m_input_tbl_nm",
        target="&m_output_tbl_nm",
        targetLib = "&m_output_caslib_nm.",
        drop=&m_drop_sess_scope_tbl_flg.
    ;
    quit;

%mend dabt_promote_table_to_cas;